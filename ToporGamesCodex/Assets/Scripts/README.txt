##Readme with an overview of the functions of all scripts, the .json implementation README is located in the BuildFolder.
This README explains the function of all scripts and how they work together with each other.
#General
	-Theres are scripts for both Codex and Notes they follow the same work structure since the Notes scripts are based on the Codex scripts.
		Diferences usually are different values and methods. Ej: The notes interface only uses 1 json file while codex uses 4 files so there's no function to switch between json files in JSONTemplateNotes.cs
	*General Script Workflow:
	1.UIStateMachine //Determines whether the current interface is Codex or Notes
		2.Codex/NotesStateMachine //Current state of the Codex or Notes: For Notes Determines the ACT and for Codex the Category
			3.JSONTemplate //Reads the appropiate json file or part of the json file depeding on the state.
				4.CreateButtons //Creates the required buttons depending on the json file.
					5.Codex/NotesButtons //Gives the button the required functions
						6.ButtonVisuals and UIElements //Changes Interface display depending on the button pressed (or highlighted) /currentstate
						
#StateMachines
	-The StateMachines have the functions to switch between states (be it Interface, Category or Act)
#JSONTemplate
	-For Codex depending on State the change 
#CreateButtons
	-Creates the buttons from prefabs
	-Have function ClearButtons() to reload the interface with the new information
	-For the CodexButtons it gives them an Int so when they're pressed they the int to UIElements and CreateButtons so they know what part of the json they have to display.
	-For NotesButton here the sentences are created depending on the current Act. Similar to the CodexButton they have an int that determines what string corresponds to the button in the string List from the json file.
		This allows the functions to delete sentece to work from NotesButtons.cs
#CodexButtons
	-GetInt() gets the int from CreateButtons
	-SendInt() sends the int to UI Elements to display information
#NotesButtons
	-AddNote() enables input field
		-SubmitNote() Adds the string to the json file 
			*If there are more than 5 the first string in the list gets deleted then adds the new string
	-SelectedNote()
		-Highlights the clickedNote
		-DeleteNote() Deletes the selected note from Json file
