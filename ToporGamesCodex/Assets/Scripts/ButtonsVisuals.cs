﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonsVisuals : MonoBehaviour
{
    //this code will be referenced in the states machines to highlight the corresponding button;
    // as well as in the CodexButtons and Notesbuttons script;
    public Sprite notpressed;
    public Sprite pressed;



    public void Pressed(GameObject button)
    {
        button.GetComponent<Image>().sprite = pressed;
    }
    public void NotPressed(GameObject button)
    {
        button.GetComponent<Image>().sprite = notpressed;
    }
    public void Highlighted(GameObject button)
    { button.transform.GetComponentInChildren<Text>().fontStyle = FontStyle.Bold; }
    public void NotHighlighted(GameObject button)
    { button.transform.GetComponentInChildren<Text>().fontStyle = FontStyle.Normal; }

}
