﻿using UnityEngine;
using UnityEngine.UI;

public class DataPathTestScript : MonoBehaviour
{
    public Text text;
    public Text text2;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        text.text = Application.dataPath;
        text2.text = System.IO.Directory.GetCurrentDirectory();
    }
}
