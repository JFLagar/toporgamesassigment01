﻿using UnityEngine;
using UnityEngine.UI;

public class CodexStateMachine : MonoBehaviour
{
    public Text subMenu;
    public States currentState;
    public CreateButtons createButtons;
    public GameObject[] menubuttons; //because there's more than 1 button 
    public ButtonsVisuals[] button; // because there's different kinds of button
    //Wrote in Mayus because we want the string for the text in SubMenu to be in Mayus;
    public enum States
    {
        CHARACTERS,
        LOCATIONS,
        ORGANIZATIONS,
        HISTORY
    }
    // Start is called before the first frame update
    void Start()
    {
        currentState = States.CHARACTERS;
    }

    // Update is called once per frame
    void Update()
    {
        subMenu.text = currentState.ToString();
        switch (currentState)
        {
            case States.CHARACTERS:
                for (int i = 0; i < 4; i++) //first we make all buttons disabled
                { button[i].NotPressed(menubuttons[i]); }
                button[0].Pressed(menubuttons[0]); //we enabled the correct one
                break;
            case States.LOCATIONS:
                for (int i = 0; i < 4; i++) //first we make all buttons disabled
                { button[i].NotPressed(menubuttons[i]); }
                button[1].Pressed(menubuttons[1]); //we enabled the correct one
                break;
            case States.ORGANIZATIONS:
                for (int i = 0; i < 4; i++) //first we make all buttons disabled
                { button[i].NotPressed(menubuttons[i]); }
                button[2].Pressed(menubuttons[2]); //we enabled the correct one
                break;
            case States.HISTORY:
                for (int i = 0; i < 4; i++) //first we make all buttons disabled
                { button[i].NotPressed(menubuttons[i]); }
                button[3].Pressed(menubuttons[3]); //we enabled the correct one
                break;
        }
    }
    public void CharactersButton()
    {
        if (currentState != States.CHARACTERS)
        {
            currentState = States.CHARACTERS;
            createButtons.topicint = 0;
            createButtons.entryint = 0;
            createButtons.ClearButtons();

        }
        else  // To avoid reloading the same window
        { return; }
    }
    public void LocationsButton()
    {
        if (currentState != States.LOCATIONS)
        {
            currentState = States.LOCATIONS;
            createButtons.topicint = 0;
            createButtons.entryint = 0;
            createButtons.ClearButtons();
        }
        else
        { return; }

    }
    public void OrganizationsButton()
    {
        if (currentState != States.ORGANIZATIONS)
        {
            currentState = States.ORGANIZATIONS;
            createButtons.topicint = 0;
            createButtons.entryint = 0;
            createButtons.ClearButtons();
        }

        else
        { return; }
    }
    public void HistoryButton()
    {
        if (currentState != States.HISTORY)
        {
            currentState = States.HISTORY;
            createButtons.topicint = 0;
            createButtons.entryint = 0;
            createButtons.ClearButtons();
        }
        else
        { return; }
    }
}
