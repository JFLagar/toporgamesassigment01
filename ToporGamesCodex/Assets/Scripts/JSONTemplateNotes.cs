﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JSONTemplateNotes : MonoBehaviour
{
    public Notes jsontest;
    private Notes writtenNote;
    public string json;
    public NotesStateMachine notesState;

    public Notes WrittenNote { get => writtenNote; set => writtenNote = value; }

    //This will generate a JSON file with the template on how to write date for the CODEX Display
    private void Awake()
    {
        if (!File.Exists(Application.persistentDataPath + "/Notes.json")) //if file doesnt exist
        {
            json = Resources.Load<TextAsset>("Data/" + "Notes").ToString();
            File.WriteAllText(Application.persistentDataPath + "/Notes.json", json);
            Debug.Log("creating file");
        }
        else
        {
            json = File.ReadAllText(Application.persistentDataPath + "/Notes.json");
        }

        WrittenNote = new Notes();
        WrittenNote = JsonUtility.FromJson<Notes>(json);
        jsontest = new Notes();
        jsontest = JsonUtility.FromJson<Notes>(json);


    }
    // Start is called before the first frame update
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {
        json = File.ReadAllText(Application.persistentDataPath + "/Notes.json");
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    string json = JsonUtility.ToJson(jsontest);
        //    File.WriteAllText(Application.dataPath + "/Data/" + "Notes.json", json);
        //}


    }

    public void OverwriteJson(Notes notes) //turns the list into the array and overwrites the jsonfile
    {

        string json = JsonUtility.ToJson(notes);
        File.WriteAllText(Application.persistentDataPath + "/Notes.json", json);
        JsonUtility.FromJsonOverwrite(json, notes);
        notesState.createButtons.ClearButtons();

    }

    [Serializable]
    public class Notes
    {

        public WrittenNotes[] writtenNotes;

    }
    [Serializable]
    public class WrittenNotes
    {

        public int act;
        public List<string> listnotes;
    }




}
