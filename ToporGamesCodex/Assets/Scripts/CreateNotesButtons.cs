﻿using UnityEngine;
using UnityEngine.UI;
public class CreateNotesButtons : MonoBehaviour
{

    public GameObject NoteButtonPrefab;
    bool buttonscleared;
    public int act = 0;
    JSONTemplateNotes template;
    JSONTemplateNotes.Notes WrittenNotes;
    // Start is called before the first frame update
    void Start()
    {
        template = GameObject.FindGameObjectWithTag("NotesTag").GetComponent<JSONTemplateNotes>();
        WrittenNotes = template.WrittenNote;
        ClearButtons();
        InstantiateNoteButtons();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount == 0) //this allows the app to breath and load the json file
        { InstantiateNoteButtons(); }
    }

    public void InstantiateNoteButtons()
    {

        if (WrittenNotes.writtenNotes[act].listnotes.Count > 0)
        {
            Debug.Log("NoteInstantiated");
            for (int i = 0; i < WrittenNotes.writtenNotes[act].listnotes.Count; i++)
            {
                GameObject Notebutton = Instantiate(NoteButtonPrefab, gameObject.transform);

                Notebutton.transform.parent = gameObject.transform;
                Notebutton.transform.localScale = new Vector3(1, 1, 1);
                Notebutton.GetComponent<NotesButtons>().noteint = i;
                Notebutton.name = ("Note" + i);
                Notebutton.transform.position = new Vector3(Notebutton.transform.position.x, Notebutton.transform.position.y - (i * 120));
                Notebutton.transform.GetComponentInChildren<Text>().text = WrittenNotes.writtenNotes[act].listnotes[i];
            }
        }
        else
        {

            return;
        }

    }
    public void ClearButtons()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        buttonscleared = true;
    }
}
