﻿using System;
using System.IO;
using UnityEngine;

public class JSONTemplate : MonoBehaviour
{

    private Category entrytest;
    public string json;
    public CodexStateMachine codexState;
    private string test;
    public Category Entrytest { get => entrytest; set => entrytest = value; }

    //This will generate a JSON file with the template on how to write date for the CODEX Display
    private void Awake()
    {
        if (!File.Exists(Application.persistentDataPath + "/Characters.json")) //if file doesnt exist
        {
            json = Resources.Load<TextAsset>("Data/" + "Characters").ToString();
            File.WriteAllText(Application.persistentDataPath + "/Characters.json", json);
            Debug.Log("Creatingfile");
        }
        else
        {
            json = File.ReadAllText(Application.persistentDataPath + "/Characters.json");
        }

        Entrytest = new Category();
        Entrytest = JsonUtility.FromJson<Category>(json);
    }
    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        switch (codexState.currentState)
        {
            case CodexStateMachine.States.CHARACTERS:
                json = File.ReadAllText(Application.persistentDataPath + "/Characters.json");

                break;
            case CodexStateMachine.States.LOCATIONS:
                if (!File.Exists(Application.persistentDataPath + "/Locations.json")) //if file doesnt exist
                {
                    json = Resources.Load<TextAsset>("Data/" + "Locations").ToString();
                    File.WriteAllText(Application.persistentDataPath + "/Locations.json", json);
                    Debug.Log("Creatingfile");
                }
                else
                {
                    json = File.ReadAllText(Application.persistentDataPath + "/Locations.json");
                }

                break;
            case CodexStateMachine.States.ORGANIZATIONS:
                if (!File.Exists(Application.persistentDataPath + "/Organizations.json")) //if file doesnt exist
                {
                    json = Resources.Load<TextAsset>("Data/" + "Organizations").ToString();
                    File.WriteAllText(Application.persistentDataPath + "/Organizations.json", json);
                    Debug.Log("Creatingfile");
                }
                else
                {
                    json = File.ReadAllText(Application.persistentDataPath + "/Organizations.json");
                }

                break;
            case CodexStateMachine.States.HISTORY:
                if (!File.Exists(Application.persistentDataPath + "/History.json")) //if file doesnt exist
                {
                    json = Resources.Load<TextAsset>("Data/" + "History").ToString();
                    File.WriteAllText(Application.persistentDataPath + "/History.json", json);
                    Debug.Log("Creatingfile");
                }
                else
                {
                    json = File.ReadAllText(Application.persistentDataPath + "/History.json");
                }

                break;
        }

        JsonUtility.FromJsonOverwrite(json, Entrytest);

        //this was made to modify the .json file from editor during playtime.
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    string json = JsonUtility.ToJson(entrytest);
        //    File.WriteAllText(Application.dataPath + "/Data/" + "testclass.json", json);
        //}
    }
    [Serializable]
    public class Category
    {
        public string category;
        public Topic[] topics;

    }
    [Serializable]
    public class Topic
    {
        public string topic;
        public Entry[] entries;
    }
    [Serializable]
    public class Entry
    {
        public string name;
        public bool containsimage;
        public string imagepath;
        public string text;

    }


}
