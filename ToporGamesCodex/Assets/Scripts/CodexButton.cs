﻿using UnityEngine;
public class CodexButton : MonoBehaviour
{
    public bool topic;
    int topicint;
    int entryint;
    public CreateButtons createButtons;
    public ButtonsVisuals buttons;
    private void Start()
    {
        createButtons = GetComponentInParent<CreateButtons>();
    }
    private void Update()
    {
        if (topic)
        {
            if (createButtons.topicint == topicint)
            { buttons.Pressed(gameObject); }
            else
            { buttons.NotPressed(gameObject); }
        }
        else
        {
            if (createButtons.entryint == entryint)
            { buttons.Highlighted(gameObject); }
            else
            { { buttons.NotHighlighted(gameObject); } }
        }
    }
    public void GetTopicint(int topic)
    {
        topicint = topic;
    }
    public void GetEntryint(int entry)
    {
        entryint = entry;
    }
    public void SendTopicint()
    {

        createButtons.topicint = topicint;
        createButtons.InstantiateEntryButtons();
        createButtons.entryint = 0; //this avoids errors like Array indx being out of reach
        //ej. lets say that in the topic 1 we have 8 entries and we last clicked in entry 7
        //but topic 2 has only 2 entries so because entry 7 doesnt exist in topic 2 there's an error
    }
    public void SendEntryint()
    {

        createButtons.entryint = entryint;
        Debug.Log("sending int");

    }

}
