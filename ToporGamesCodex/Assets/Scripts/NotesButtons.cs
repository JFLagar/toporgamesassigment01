﻿using UnityEngine;
using UnityEngine.EventSystems;

using UnityEngine.UI;
public class NotesButtons : MonoBehaviour, IDeselectHandler
{
    public bool selected = false;
    public int noteint;
    public GameObject DeleteButton;
    public InputField inputfield;

    public JSONTemplateNotes templateNotes;
    public GameObject newButton;
    public ButtonsVisuals buttonsVisuals;
    public string pointername;
    public GameObject eventSystem; //we want pointername to be update everytime we click on any element not just this button
    private void Start()
    {

        templateNotes = GameObject.FindGameObjectWithTag("NotesTag").GetComponent<JSONTemplateNotes>();
        eventSystem = GameObject.Find("EventSystem");
    }
    private void Update()
    {

        if (selected)
        {
            if (eventSystem.GetComponent<EventSystem>().currentSelectedGameObject != null)
            {
                pointername = eventSystem.GetComponent<EventSystem>().currentSelectedGameObject.name;
                if (pointername != gameObject.transform.parent.name) //checks if the last object selecte was the parent that spawned this button
                {
                    if (pointername != gameObject.transform.name) //check hat the button after pressing the parent was the destroy button
                    { Destroy(gameObject, 0); }

                }

            }
        }

    }

    public void SelectNote()
    {
        newButton = Instantiate(DeleteButton, gameObject.transform);  //HighlightNote
        buttonsVisuals.Highlighted(gameObject);
        newButton.SetActive(true);


    }
    public void DeleteNote()
    {
        Debug.Log("Delete");
        templateNotes.WrittenNote.writtenNotes[templateNotes.notesState.createButtons.act].listnotes.RemoveAt(transform.parent.GetComponent<NotesButtons>().noteint);//DeleteNote FROM JSON    
        templateNotes.OverwriteJson(templateNotes.WrittenNote);
        Destroy(gameObject, 0f);
    }
    public void AddNote() //just for the AddNotebutton
    {
        inputfield.text = "Write here";
        inputfield.interactable = true;
        inputfield.ActivateInputField();


    }
    public void SaveNote() //to add to the JSON
    {
        if (Input.GetButtonDown("Submit"))
        {
            Debug.Log("Saved");
            if (templateNotes.WrittenNote.writtenNotes[templateNotes.notesState.createButtons.act].listnotes.Count < 5)
            { templateNotes.WrittenNote.writtenNotes[templateNotes.notesState.createButtons.act].listnotes.Add(inputfield.text); }
            else
            {
                templateNotes.WrittenNote.writtenNotes[templateNotes.notesState.createButtons.act].listnotes.RemoveAt(0);
                templateNotes.WrittenNote.writtenNotes[templateNotes.notesState.createButtons.act].listnotes.Add(inputfield.text);
            }
            templateNotes.OverwriteJson(templateNotes.WrittenNote);
        }
        inputfield.text = "";

    }
    public void OnDeselect(BaseEventData data)
    {

        if (DeleteButton != null)
        {
            buttonsVisuals.NotHighlighted(this.gameObject);
        }


    }



}
