﻿using UnityEngine;
using UnityEngine.UI;
public class UIElements : MonoBehaviour
{
    public Text entryTitle;
    public Text entryName;
    public GameObject image;
    public Sprite sprite;
    public Text entryText;
    public bool hasimage;
    int currententry;
    int currenttopic;
    JSONTemplate template;
    JSONTemplate.Category Entrytest;
    // Start is called before the first frame update
    void Start()
    {
        template = GameObject.FindGameObjectWithTag("CodexTag").GetComponent<JSONTemplate>();
        Entrytest = template.Entrytest;
        Resources.Load<Sprite>(Entrytest.topics[currenttopic].entries[currententry].imagepath);
        Debug.Log(Entrytest.topics[currenttopic].entries[currententry].imagepath);
    }

    // Update is called once per frame
    void Update()
    {

        CreateButtons createButtons = GetComponentInChildren<CreateButtons>();
        currententry = createButtons.entryint;
        currenttopic = createButtons.topicint;
        hasimage = Entrytest.topics[currenttopic].entries[currententry].containsimage;
        entryTitle.text = Entrytest.topics[currenttopic].entries[currententry].name;
        entryName.text = Entrytest.topics[currenttopic].entries[currententry].name;
        entryText.text = Entrytest.topics[currenttopic].entries[currententry].text;
        if (hasimage)
        {
            image.SetActive(true);
            //image = Resources.Load(entrytest.topics[currenttopic].entries[currententry].imagepath) as Image;
            image.GetComponent<Image>().sprite = Resources.Load<Sprite>(Entrytest.topics[currenttopic].entries[currententry].imagepath);

        }
        else
        {
            image.SetActive(false);

        }





    }
}
