﻿using UnityEngine;
using UnityEngine.UI;

public class CreateButtons : MonoBehaviour
{
    public GameObject TopicButtonPrefab;
    public GameObject EntryButtonPrefab;
    bool buttonscleared;
    public int topicint = 0;
    public int entryint = 0;
    JSONTemplate template;
    JSONTemplate.Category Entrytest;
    // Start is called before the first frame update
    void Start()
    {
        template = GameObject.FindGameObjectWithTag("CodexTag").GetComponent<JSONTemplate>();
        Entrytest = template.Entrytest;
        InstantiateTopicButtons();
        InstantiateEntryButtons();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount == 0) //this allows the app to breath and load the json file
        { InstantiateBoth(); }
    }
    public void InstantiateTopicButtons()
    {
        if (Entrytest.topics.Length > 0) //avoid crashes in clase blank entries
        {
            for (int i = 0; i < Entrytest.topics.Length; i++)
            {

                GameObject Topicbutton = Instantiate(TopicButtonPrefab, gameObject.transform);
                Topicbutton.transform.parent = gameObject.transform;
                Topicbutton.transform.localScale = new Vector3(1, 1, 1);
                Topicbutton.GetComponent<CodexButton>().GetTopicint(i);
                Topicbutton.name = ("Topic " + i);
                Topicbutton.transform.position = new Vector3(Topicbutton.transform.position.x, Topicbutton.transform.position.y - (i * 60));
                Topicbutton.transform.GetComponentInChildren<Text>().text = Entrytest.topics[i].topic;
            }
        }
        else
        { return; }


    }
    public void InstantiateEntryButtons()
    {
        ClearButtons();
        InstantiateTopicButtons();
        if (Entrytest.topics[topicint].entries.Length > 0)
        {
            for (int i = 0; i < Entrytest.topics[topicint].entries.Length; i++)
            {
                GameObject Entrybutton = Instantiate(EntryButtonPrefab, gameObject.transform);

                Entrybutton.transform.parent = gameObject.transform;
                Entrybutton.transform.localScale = new Vector3(1, 1, 1);
                Entrybutton.GetComponent<CodexButton>().GetEntryint(i);
                Entrybutton.name = ("Entry " + i);
                Entrybutton.transform.position = new Vector3(Entrybutton.transform.position.x, Entrybutton.transform.position.y - (i * 50));
                Entrybutton.transform.GetComponentInChildren<Text>().text = Entrytest.topics[topicint].entries[i].name;
            }
        }
        else
        { return; }
    }
    public void ClearButtons()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        buttonscleared = true;
    }
    public void InstantiateBoth()
    {
        ClearButtons();
        InstantiateTopicButtons();
        InstantiateEntryButtons();
    }
}
