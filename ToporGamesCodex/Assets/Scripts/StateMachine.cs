﻿using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public GameObject CodexCanvas; //GameObject containing the Codex Menu
    public GameObject NotesCanvas; //GameObject containing the Note Menu
    public GameObject codexButton;
    public GameObject notesButton;
    public ButtonsVisuals button;
    public MenuState currentState; //Shows active menu

    public enum MenuState
    {
        Codex,
        Notes
    }


    //We start the game in the Codex Menu
    void Start()
    {
        currentState = MenuState.Codex;
    }
    //Depending on the current State we see one menu or other, due to being only 2 big Menus we can go for a If else without issue.
    private void Update()
    {
        if (currentState == MenuState.Codex)
        {
            NotesCanvas.SetActive(false);
            CodexCanvas.SetActive(true);
            button.Pressed(codexButton);
            button.NotPressed(notesButton);
        }
        else if (currentState == MenuState.Notes)
        {
            CodexCanvas.SetActive(false);
            NotesCanvas.SetActive(true);
            button.Pressed(notesButton);
            button.NotPressed(codexButton);

        }
        //if order to exit the game in fullscreen mode.
        if (Input.GetKeyDown(KeyCode.Escape))
        { QuitGame(); }
    }
    //Fuctions for the UI Buttons
    public void CodexButton()
    {
        currentState = MenuState.Codex;
        Debug.Log(gameObject.name);
    }
    public void NotesButton()
    {
        currentState = MenuState.Notes;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
