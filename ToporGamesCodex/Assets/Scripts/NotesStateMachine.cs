﻿using UnityEngine;

public class NotesStateMachine : MonoBehaviour
{

    public Act currentAct;
    public CreateNotesButtons createButtons;
    public GameObject[] menubuttons; //because there's more than 1 identical button 
    public ButtonsVisuals button;
    //Wrote in Mayus because we want the string for the text in SubMenu to be in Mayus;
    public enum Act
    {
        I,
        II,
        III,
        IV
    }
    // Start is called before the first frame update
    void Start()
    {
        currentAct = Act.I;
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentAct)
        {
            case Act.I:
                foreach (GameObject gameObject in menubuttons) //first we make all buttons disabled
                { button.NotPressed(gameObject); }
                button.Pressed(menubuttons[0]); //we enabled the correct one
                break;
            case Act.II:
                foreach (GameObject gameObject in menubuttons) //first we make all buttons disabled
                { button.NotPressed(gameObject); }
                button.Pressed(menubuttons[1]); //we enabled the correct one
                break;
            case Act.III:
                foreach (GameObject gameObject in menubuttons) //first we make all buttons disabled
                { button.NotPressed(gameObject); }
                button.Pressed(menubuttons[2]); //we enabled the correct one
                break;
            case Act.IV:
                foreach (GameObject gameObject in menubuttons) //first we make all buttons disabled
                { button.NotPressed(gameObject); }
                button.Pressed(menubuttons[3]); //we enabled the correct one
                break;
        }
    }
    public void ActIButton()
    {
        if (currentAct != Act.I)
        {
            currentAct = Act.I;
            createButtons.ClearButtons();
            createButtons.act = 0;
        }
        else
        { return; }
    }
    public void ActIIButton()
    {
        if (currentAct != Act.II)
        {
            currentAct = Act.II;
            createButtons.ClearButtons();
            createButtons.act = 1;

        }
        else
        { return; }
    }
    public void ActIIIButton()
    {
        if (currentAct != Act.III)
        {
            currentAct = Act.III;
            createButtons.ClearButtons();
            createButtons.act = 2;
        }
        else
        { return; }
    }
    public void ActIVButton()
    {
        if (currentAct != Act.IV)
        {
            currentAct = Act.IV;
            createButtons.ClearButtons();
            createButtons.act = 3;
        }
        else
        { return; }

    }
}
