﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{

    public class NotesJsonReading
    {
        private Game game;
        private NotesStateMachine notesstate;
        private JSONTemplateNotes template;

        [UnityTest]
        public IEnumerator ChangeToNotes() //First IEnumerator required to change the UI to Notes
        {
            GameObject gameGameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Game"));
            game = gameGameObject.GetComponent<Game>();

            StateMachine stateMachine = GameObject.Find("Top").GetComponent<StateMachine>();
            stateMachine.NotesButton();

            yield return new WaitForSeconds(1); //to let the interface load.
            LogAssert.Equals(true, stateMachine.NotesCanvas.active);


        }
        [UnityTest]
        public IEnumerator JsonExist() //We check that we file exist
        {
            //we always have to get the component again because we "reset" the game every time a button is presss to load the interface
            notesstate = GameObject.Find("Top").GetComponent<StateMachine>().NotesCanvas.GetComponent<NotesStateMachine>();
            template = GameObject.Find("Top").GetComponent<StateMachine>().NotesCanvas.GetComponent<JSONTemplateNotes>();

            yield return null;
            LogAssert.Equals(File.Exists(Application.persistentDataPath + "/Notes.json"), true);
        }

        [UnityTest]
        public IEnumerator JsonRead() //Check that we are reading the file
        {
            template = GameObject.Find("Top").GetComponent<StateMachine>().NotesCanvas.GetComponent<JSONTemplateNotes>();
            yield return null;
            LogAssert.Equals(File.ReadAllText(Application.persistentDataPath + "/Notes.json"), template.json);
        }

    }
}
