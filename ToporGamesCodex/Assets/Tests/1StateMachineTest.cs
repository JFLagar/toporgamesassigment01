﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class StateMachineTest
    {
        private Game game; //this instantiates the whole game

        [UnityTest]
        public IEnumerator LaunchGame()
        {
            GameObject gameGameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Game")); //instantiate the game object
            game = gameGameObject.GetComponent<Game>();
            yield return null;
            LogAssert.Equals(game.isActiveAndEnabled.Equals(true), game.isActiveAndEnabled); //game has loaded


        }
        [UnityTest]
        public IEnumerator UIStateAtBeginningofGame()
        {

            StateMachine stateMachine = GameObject.Find("Top").GetComponent<StateMachine>(); //get state machine
            yield return null;
            LogAssert.Equals(StateMachine.MenuState.Codex, stateMachine.currentState); //check that the game starts at codex
        }
        [UnityTest]
        public IEnumerator NotesButton()
        {

            StateMachine stateMachine = GameObject.Find("Top").GetComponent<StateMachine>();
            stateMachine.NotesButton(); //function that changes State, ergo Interface
            yield return null;
            LogAssert.Equals(StateMachine.MenuState.Notes, stateMachine.currentState); //check that the game is at Notes now

        }
        [UnityTest]
        public IEnumerator CodexButton()
        {

            StateMachine stateMachine = GameObject.Find("Top").GetComponent<StateMachine>();

            stateMachine.CodexButton(); //change again to codex
            yield return null;
            LogAssert.Equals(StateMachine.MenuState.Codex, stateMachine.currentState); //changes to codex

        }
        [UnityTest]
        public IEnumerator SameButton()
        {

            StateMachine stateMachine = GameObject.Find("Top").GetComponent<StateMachine>();
            stateMachine.NotesButton(); //go to notes
            stateMachine.NotesButton(); // already in notes
            yield return null;
            LogAssert.Equals(StateMachine.MenuState.Notes, stateMachine.currentState); //check that state stays at notes
        }

    }
}
