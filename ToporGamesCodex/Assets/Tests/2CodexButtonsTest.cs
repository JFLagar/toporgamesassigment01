﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class CodexButtonsTest
    {
        private Game game;
        private CodexStateMachine codexState;

        [UnityTest]
        public IEnumerator CharacterButton()
        {
            GameObject gameGameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Game"));
            game = gameGameObject.GetComponent<Game>();
            codexState = GameObject.Find("CodexCanvas").GetComponent<CodexStateMachine>(); //get state machine with button function
            codexState.CharactersButton(); //changes state to character, but since it already is nothign happens
            yield return null;
            LogAssert.Equals(CodexStateMachine.States.CHARACTERS, codexState.currentState); //chekc if we are in character
        }


        [UnityTest]
        public IEnumerator LocationButton()
        {

            codexState.LocationsButton(); //change to locations
            yield return null;
            LogAssert.Equals(CodexStateMachine.States.LOCATIONS, codexState.currentState); //check if we are in proper state
        }
        [UnityTest]
        public IEnumerator OrganizationsButton()
        {

            codexState.OrganizationsButton();
            yield return null;
            LogAssert.Equals(CodexStateMachine.States.ORGANIZATIONS, codexState.currentState);
        }
        [UnityTest]
        public IEnumerator HistoryButton()
        {

            codexState.HistoryButton();
            yield return null;
            LogAssert.Equals(CodexStateMachine.States.HISTORY, codexState.currentState);
        }
        [UnityTest]
        public IEnumerator TopicButton()
        {
            CreateButtons buttons = GameObject.Find("SubMenus").GetComponent<CreateButtons>();
            CodexButton codexButton = GameObject.Find("Topic 3").GetComponent<CodexButton>();

            codexButton.SendTopicint(); //check that this button is getting the proper int to change topic group and spawn the proper topics
            yield return null;
            LogAssert.Equals(buttons.topicint, codexButton.createButtons.topicint); //check if it is the proper int
        }
        [UnityTest]
        public IEnumerator EntryButton()
        {
            CreateButtons buttons = GameObject.Find("SubMenus").GetComponent<CreateButtons>();
            CodexButton codexButton = GameObject.Find("Entry 2").GetComponent<CodexButton>();

            codexButton.SendEntryint(); //same as topic but this one shows the entry information
            yield return null;
            LogAssert.Equals(buttons.entryint, codexButton.createButtons.entryint); //check if it is the proper int
        }

    }
}
