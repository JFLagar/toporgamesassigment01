﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class NotesButtonsTest
    {
        private Game game;
        private StateMachine stateMachine;
        private NotesStateMachine notesState;

        [UnityTest]
        public IEnumerator ChangeToNotes() //First IEnumerator required to change the UI to Notes
        {
            GameObject gameGameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Game"));
            game = gameGameObject.GetComponent<Game>();

            stateMachine = GameObject.Find("Top").GetComponent<StateMachine>();
            stateMachine.NotesButton();
            notesState = stateMachine.NotesCanvas.GetComponent<NotesStateMachine>();
            yield return new WaitForSeconds(1);
            LogAssert.Equals(true, stateMachine.NotesCanvas.active);


        }
        [UnityTest]
        public IEnumerator Act2Button() // buttons that change states , starts at 2 because the default act is one
        {
            //we have to get the component again since we restart the interface to load the new data

            notesState = GameObject.Find("Top").GetComponent<StateMachine>().NotesCanvas.GetComponent<NotesStateMachine>();
            notesState.ActIIButton();
            yield return null;
            LogAssert.Equals(NotesStateMachine.Act.II, notesState.currentAct); //check if its the right state
        }
        [UnityTest]
        public IEnumerator Act1Button()
        {


            notesState = GameObject.Find("Top").GetComponent<StateMachine>().NotesCanvas.GetComponent<NotesStateMachine>();
            notesState.ActIButton();
            yield return null;
            LogAssert.Equals(NotesStateMachine.Act.I, notesState.currentAct);
        }
        [UnityTest]
        public IEnumerator Act3Button()
        {


            notesState = GameObject.Find("Top").GetComponent<StateMachine>().NotesCanvas.GetComponent<NotesStateMachine>();
            notesState.ActIIIButton();
            yield return null;
            LogAssert.Equals(NotesStateMachine.Act.III, notesState.currentAct);
        }
        [UnityTest]
        public IEnumerator Act4Button()
        {


            notesState = GameObject.Find("Top").GetComponent<StateMachine>().NotesCanvas.GetComponent<NotesStateMachine>();
            notesState.ActIVButton();
            yield return null;
            LogAssert.Equals(NotesStateMachine.Act.IV, notesState.currentAct);
        }
    }
}
