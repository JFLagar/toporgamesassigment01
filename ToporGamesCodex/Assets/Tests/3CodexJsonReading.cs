﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class CodexJsonReading
    {
        private Game game;
        private CodexStateMachine codexState;
        private JSONTemplate template;

        [UnityTest]
        public IEnumerator JsonExist() //check that file exist
        {
            GameObject gameGameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Game"));
            game = gameGameObject.GetComponent<Game>();
            codexState = GameObject.Find("CodexCanvas").GetComponent<CodexStateMachine>();
            template = GameObject.Find("CodexCanvas").GetComponent<JSONTemplate>();

            yield return null;
            LogAssert.Equals(File.Exists(Application.persistentDataPath + "/Characters.json"), true);
        }
        [UnityTest]
        public IEnumerator JsonStartCharacters() //check that when the state changes the json file changes
        {
            codexState.CharactersButton();
            yield return null;
            LogAssert.Equals(File.ReadAllText(Application.persistentDataPath + "/Characters.json"), template.json);
        }
        [UnityTest]
        public IEnumerator JsonSwitchLocations()
        {
            codexState.LocationsButton();
            yield return null;
            LogAssert.Equals(File.ReadAllText(Application.persistentDataPath + "/Locations.json"), template.json);
        }
        [UnityTest]
        public IEnumerator JsonSwitchCharacters()
        {
            codexState.CharactersButton();
            yield return null;
            LogAssert.Equals(File.ReadAllText(Application.persistentDataPath + "/Characters.json"), template.json);
        }
        [UnityTest]
        public IEnumerator JsonSwitchOrganizations()
        {
            codexState.CharactersButton();
            yield return null;
            LogAssert.Equals(File.ReadAllText(Application.persistentDataPath + "/Organizations.json"), template.json);
        }
        public IEnumerator JsonSwitchHistory()
        {
            codexState.CharactersButton();
            yield return null;
            LogAssert.Equals(File.ReadAllText(Application.persistentDataPath + "/History.json"), template.json);
        }
    }
}
